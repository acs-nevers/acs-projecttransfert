<?php
require('models/model.php');

// $data = downloadFile($url[1])['data'];
//$data = base64_decode($data);

$url = explode('/', $_GET['url']);
$lastElem = count($url) - 1;
$link = downloadFile($url[$lastElem])['link'];

require('views/download.php');

if(isset($_POST['download_button']))
{
	download();
}

function download()
{
	// header('Content-Type: application/zip');
	// header("Content-Transfer-Encoding: binary");
	// header('Content-Length: '.filesize($f));
	// header('Content-Disposition: attachment; filename='.$f);
	// header('Pragma: no-cache');
	// header("Cache-Control: no-cache, must-revalidate");
	// header('Expires: 0');
	// ob_clean();
	// readfile($d);
	// ob_flush();
	// exit();

	global $link;
	$file = 'upload/'.$link.'.zip';
	header('Content-Description: File Transfer');
    header('Content-Type: application/zip');
    header('Content-Disposition: attachment; filename="'.$link.'.zip"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    readfile($file);
    ob_flush();
    exit;
}
?>