function gid(element) {
  return document.getElementById(element);
}

gid('copy-link').addEventListener('click', function() {
  gid('link').select();
  document.execCommand('copy');
});

gid('follow-link').addEventListener('click', function() {
  var link = 'download/' + gid('link-section').getAttribute('data-link');
  document.location.href = link;
});
