/*eslint-env browser*/

function EId(x) {
	return document.getElementById(x);
}

var wrongMail1 = 1;
var wrongMail2 = 1;

EId('upload-file').addEventListener('change', function (e) {
	var hint = EId('upload-status').innerHTML;
	var fileName = '';
	if (this.files && this.files.length > 1) {
		fileName = (this.getAttribute('data-multiple-caption') ||
			'').replace('{count}', this.files.length);
	} else {
		fileName = e.target.value.split('\\').pop();
	}
	if (fileName) {
		EId('upload-status').innerHTML = fileName;
	} else {
		EId('upload-status').innerHTML = hint;
	}
});

EId('email-sender').addEventListener('change', function () {
	if (RegExp(/^[\w.-]+@[\w.-]{2,}\.[a-z]{2,6}$/).test(EId('email-sender').value) == true) {
		wrongMail1 = 0;
	} else {
		wrongMail1 = 1;
	}
});

EId('email-receiver').addEventListener('change', function () {
	if (RegExp(/^[\w.-]+@[\w.-]{2,}\.[a-z]{2,6}$/).test(EId('email-receiver').value) == true) {
		wrongMail2 = 0;
	} else {
		wrongMail2 = 1;
	}
});

var overlay = EId('overlay');

overlay.addEventListener('click', function (){
	overlay.classList.remove('active');
});

EId('submit-button').addEventListener('click', function (event)
{
	if (EId('upload-file').value == '')
	{
		event.preventDefault();
		overlay.classList.add('active');
		EId('wrong1').innerHTML = 'Veuillez ajouter un fichier';
	}
	else{
			EId('wrong1').innerHTML = '';
	}

	if (wrongMail1 + wrongMail2 > 0)
	{
		event.preventDefault();
		overlay.classList.add('active');
		EId('wrong2').innerHTML = 'adresse(s) mail incorrecte(s)';
	}
	else{
		EId('wrong2').innerHTML = '';
	}
});
