<?php
	require "views/header.php";
?>

<section id="link-section" data-link="<?php  echo $url[1]; ?>" class="window light">
	<p>Félicitations, vos fichiers ont été ajoutés !</p>
	<p>Voici le lien pour les télécharger :</p>
	<input type="text" name="link" id="link" readonly="readonly" value=" <?php echo base() . 'download/'.$url[1]; ?>" />
	<button id="follow-link">Accéder au lien</button>
	<p id="link-info">
		Cliquez pour
		<button id="copy-link">
			<img src="public/img/copy.svg">
		</button>
		copier de lien
	</p>
</section>

<?php
	require "views/footer.php";
?>
