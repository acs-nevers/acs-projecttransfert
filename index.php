<?php

if(isset($_GET['url']) == true)
{
	$url = explode('/', $_GET['url']);
	switch($url[0])
	{
		case '' :
			require('controllers/controller_home.php');
		break;

		case 'download' :
			require('controllers/controller_download.php');
		break;

		case 'result' :
			require('controllers/controller_result.php');
		break;

		default :
			require('views/404.php');
	}

}
else
{
	require('controllers/controller_home.php');
}
