# FileTransfer

Create a file named **connection.php** in the **tools** folder with the following code :

```php
<?php

function connect($type) {
  if ($type == 'local') {
    return [
      'BDD' => '',
      'LOGIN' => '',
      'PSWD' => ''
    ];
  }
  else {
    return [
      'BDD' => '',
      'LOGIN' => '',
      'PSWD' => ''
    ];
  }
}
?>
```
Fill in the database name and your login informations for both your local database and the production one.
