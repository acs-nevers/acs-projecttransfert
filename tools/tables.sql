#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: receivers
#------------------------------------------------------------

CREATE TABLE receivers(
        id_receiver    Int  Auto_increment  NOT NULL ,
        email_receiver Varchar (63) NOT NULL
	,CONSTRAINT receivers_PK PRIMARY KEY (id_receiver)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: senders
#------------------------------------------------------------

CREATE TABLE senders(
        id_sender    Int  Auto_increment  NOT NULL ,
        email_sender Varchar (63) NOT NULL
	,CONSTRAINT senders_PK PRIMARY KEY (id_sender)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: messages
#------------------------------------------------------------

CREATE TABLE messages(
        id_message Int  Auto_increment  NOT NULL ,
        message    Text NOT NULL
	,CONSTRAINT messages_PK PRIMARY KEY (id_message)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: files
#------------------------------------------------------------

CREATE TABLE files(
        id_file         Int  Auto_increment  NOT NULL ,
        file            Longblob NOT NULL ,
        file_name       Varchar (63) NOT NULL ,
        upload_date     Datetime NOT NULL ,
        expiration_date Datetime NOT NULL ,
        id_sender       Int NOT NULL ,
        id_message      Int
	,CONSTRAINT files_PK PRIMARY KEY (id_file)

	,CONSTRAINT files_senders_FK FOREIGN KEY (id_sender) REFERENCES senders(id_sender)
	,CONSTRAINT files_messages0_FK FOREIGN KEY (id_message) REFERENCES messages(id_message)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: have receiver
#------------------------------------------------------------

CREATE TABLE have_receiver(
        id_receiver Int NOT NULL ,
        id_file     Int NOT NULL
	,CONSTRAINT have_receiver_PK PRIMARY KEY (id_receiver,id_file)

	,CONSTRAINT have_receiver_receivers_FK FOREIGN KEY (id_receiver) REFERENCES receivers(id_receiver)
	,CONSTRAINT have_receiver_files0_FK FOREIGN KEY (id_file) REFERENCES files(id_file)
)ENGINE=InnoDB;

